let addBtn = document.querySelector(".timer__btn--add");
let startBtn = document.querySelector(".timer__btn--start");
let resetBtn = document.querySelector(".timer__btn--reset");
let laps = document.querySelector(".timer__laps");
let minutes = document.querySelector(".timer__minutes");
let seconds = document.querySelector(".timer__seconds");
let milliSeconds = document.querySelector(".timer__milliSeconds");
let isStart = false;
let isReset = false;
let milliSecond;
let minuteCounter = 0;
let secondCounter = 0;
let milliSecondCounter = 0;
let lapsArr = [];

let times = JSON.parse(localStorage.getItem("date_time"));
let arr = JSON.parse(localStorage.getItem("laps_Arr"));
if (arr) {
  lapsArr = arr;
  arr.forEach((item) => {
    print(item.minuteCounter, item.secondCounter, item.milliSecondCounter);
  });
}
function print(minut, second, miliSecond) {
  const li = document.createElement("li");
  li.setAttribute("class", "timer__lap");
  li.innerHTML += `<p class="timer__lap--text">${check(minut)}:${check(
    second
  )},${check(miliSecond)}</p>
  <button class="timer__close" type="submit">
    <img  src="close.png" width="40" height="40" alt="close__btn" />
  </button>`;
  laps.append(li);
  const lap = document.querySelectorAll(".timer__lap");
  lap.forEach((l) => {
    l.children[1].addEventListener("click", () => {
      laps.removeChild(l);
    });
  });
}
addBtn.setAttribute("disabled", "");
if (times) {
  minutes.innerHTML = `${check(times?.minuteCounter)}`;
  seconds.innerHTML = `${check(times?.secondCounter)}`;
  milliSeconds.innerHTML = `${check(times?.milliSecondCounter)}`;
  minuteCounter = `${times?.minuteCounter}`;
  secondCounter = `${times?.secondCounter}`;
  milliSecondCounter = `${times?.milliSecondCounter}`;
} else {
  minutes.innerHTML = "00";
  seconds.innerHTML = "00";
  milliSeconds.innerHTML = "00";
}

const play = () => {
  if (!isStart && !isReset) {
    startBtn.innerHTML = "Pause";
    addBtn.removeAttribute("disabled");
    milliSecond = setInterval(() => {
      milliSeconds.innerHTML = `${check(++milliSecondCounter)}`;

      if (milliSecondCounter === 100) {
        milliSecondCounter = 0;
        milliSeconds.innerHTML = `${check(milliSecondCounter)}`;
        secondCounter++;
        seconds.innerHTML = `${check(secondCounter)}`;
      }
      if (secondCounter === 60) {
        secondCounter = 0;
        seconds.innerHTML = `${check(secondCounter)}`;
        minuteCounter++;
        minutes.innerHTML = `${check(minuteCounter)}`;
      }
    }, 10);

    isStart = true;
    isReset = true;
  } else {
    startBtn.innerHTML = "Start";
    addBtn.setAttribute("disabled", true);
    clearInterval(milliSecond);
    isStart = false;
    isReset = false;
    let time = {
      minuteCounter,
      secondCounter,
      milliSecondCounter,
    };
    localStorage.setItem("date_time", JSON.stringify(time));
  }
};
const reset = () => {
  addBtn.setAttribute("disabled", true);
  isReset = true;
  // play();
  minutes.innerHTML = "00";
  seconds.innerHTML = "00";
  milliSeconds.innerHTML = "00";
  minuteCounter = 0;
  secondCounter = 0;
  milliSecondCounter = 0;
  laps.innerHTML = "";
  localStorage.clear();
};
function lap() {
  const li = document.createElement("li");
  li.setAttribute("class", "timer__lap");
  li.innerHTML += `<p class="timer__lap--text">${check(minuteCounter)}:${check(
    secondCounter
  )},${check(milliSecondCounter)}</p>
  <button class="timer__close" type="submit">
    <img  src="close.png" width="40" height="40" alt="close__btn" />
  </button>`;
  laps.append(li);
  const lap = document.querySelectorAll(".timer__lap");
  lap.forEach((l) => {
    l.children[1].addEventListener("click", () => {
      laps.removeChild(l);
    });
  });

  let lapsItem = {
    minuteCounter,
    secondCounter,
    milliSecondCounter,
  };
  lapsArr.push(lapsItem);
  localStorage.setItem("laps_Arr", JSON.stringify(lapsArr));
}
//Event
startBtn.addEventListener("click", play);
resetBtn.addEventListener("click", reset);
addBtn.addEventListener("click", lap);
//check
function check(val) {
  var valString = val + "";
  if (valString.length < 2) {
    return "0" + valString;
  } else {
    return valString;
  }
}
